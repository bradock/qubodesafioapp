FRANCISCO HUGO C�SAR - DESAFIO QUBO

EXECU��O:

1� Crie um diret�rio e execute o git clone.

link: ...

2� No diret�rio do projeto chamado 'qubo', execute no terminal o comando NPM INSTALL para fazer o download das depend�ncias do package.json

3� Abra o emulador ANDROID ou IOS

4� No diret�rio do projeto chamado 'qubo', execute no terminal o comando REACT-NATIVE RUN-ANDROID ou  REACT-NATIVE RUN-IOS (respectivo ao emulador aberto)

5� Aplicativo ser� renderizado no emulador.


DOCUMENTA��O:
Subdividir o projeto em p�ginas e arquivos:

	Actions: Referente as actions (usando Redux) do projeto
	Componentes: Componentes usados pelo aplicativo
	Redurcers: Conversa diretamento com a store e as actions (Redux)
	screens:  Referente as p�ginas do aplicatiovo
	style:	  Referente ao StyleSheet de todo o aplicativo
	utils:	  Referente as Fonts e o combineReducers
	AppNavigator: Referente a navega��o do aplicativo



