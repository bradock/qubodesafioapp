import {combineReducers} from 'redux'
import ChatReducer from '../reducers/ChatReducer'
import DashReducer from '../reducers/DashReducer'
import HomeReducer from '../reducers/HomeReducer'

const Reducers = combineReducers({
    ChatReducer:ChatReducer,
    DashReducer:DashReducer,
    HomeReducer:HomeReducer
});

export default Reducers;