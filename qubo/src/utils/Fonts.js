export const Fonts = {
    Black: 'Pangram-Black',
    Bold:  'Pangram-Bold',
    Light: 'Pangram-ExtraLight'
}