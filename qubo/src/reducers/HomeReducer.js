const initialState = {
    bars: [
        {key: 1, nome: "Revenue",       sub: "$1.8M",   rect: 170, up: true},
        {key: 2, nome: "Cash Spent",    sub: "-$0.6M ", rect: 100,  up: false},
        {key: 3, nome: "Cash Tax Paid", sub: "-$0.3M",  rect: 50,  up: false},
        {key: 4, nome: "Inventory ",    sub: "$1M",     rect: 150, up: true},
        {key: 5, nome: "Inventory ",    sub: "$0.5M",   rect: 120, up: true},
        {key: 6, nome: "Inventory ",    sub: "$0.5M",   rect: 120, up: true}
    ]
}

const HomeReducer = (state = [], action) => {
    if(state.length == 0 ){
        return initialState;
    }

    return state;
}

export default HomeReducer;