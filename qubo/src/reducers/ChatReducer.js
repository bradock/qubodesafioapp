const initialState = {
    contador : 6,
    chat: [
        {key:'1',time:'1:37 PM',msg: 'Hello, John! My name is Qbot and I’m here to help you. What do you want me to do? ',r:true},
        {key:'2',time:'1:38 PM',msg: 'Show my revenue in the last year ',r:false},
        {key:'3',time:'1:38 PM',msg: 'Ok! Rendering your data...',r:true},
        {key:'4',time:'1:38 PM',msg: 'What will be my revenue if I increase sales in 10% in the next year?',r:false},
        {key:'5',time:'1:48 PM',msg: 'There are many variations of passages of Lorem Ipsum available, but the majority',r:true},
        {key:'6',time:'1:58 PM',msg: 'Good Bye. The standard chunk of Lorem Ipsum used since the 1500s',r:false},
    ],
    helpMsg: [
        {key:'1',msg: 'Show my revenue last month'},
        {key:'2',msg: 'Show my profit in the last month'},
        {key:'3',msg: 'Lorem, Lorem'},
        {key:'4',msg: 'Qbot Qubo'},
    ],
    textValue: ""
}


const ChatReducer = (state = [], action) => {

    if(state.length == 0 ){
        return initialState;
    }

    if(action.type == 'editTextInput'){
        return { ... state, textValue: action.payload.textValue}
    }

    if(action.type == 'insertChat'){
        state.contador = state.contador + 1;
        var newMsg = {key: state.contador,time:'00:00 AM',msg: action.payload.textValue ,r:false}
        state.chat.push(newMsg)
        state.textValue = ""
        return { ... state}
    }

    return state;
};

export default ChatReducer;
