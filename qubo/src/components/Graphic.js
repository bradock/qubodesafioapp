import React, {Component} from 'react'
import {Text,View,Animated} from 'react-native'
import * as shape from 'd3-shape'
import { Circle, G, Line, Rect,Svg } from 'react-native-svg'

export default class Graphic extends Component {

    state = {
      opacity: new Animated.Value(0)
    }


    render(){

      Animated.timing(
        this.state.opacity,{
          toValue: 1,
          duration: 3000
        }
      ).start()
        
        var lines = [
            [0,0,500,0],
            [0,50,500,50],
            [0,100,500,100],    
            [0,150,500,150],
            [0,200,500,200],    
        ]
  
        var points = [
          [30,50,120,150],
          [120,150,200,30],
          [200,30,280,10],
          [280,10,310,130],
          [310,130,310,130],
      ]

        return(
             <Animated.View style={{opacity:this.state.opacity, marginTop: 60}}>
                <Svg width={500} height={200} style={{margin:5}}>
                  <G x={0}>
                    {lines.map((prop, key) => {
                      return (
                        <Line
                        x1={prop[0]}
                        y1={prop[1]}
                        x2={prop[2]}
                        y2={prop[3]}
                        stroke="gray"
                        strokeWidth="1"
                        />
                      )
                    })}

                  {points.map((prop, key) => {
                      return (
                        <Line
                        x1={prop[0]}
                        y1={prop[1]}
                        x2={prop[2]}
                        y2={prop[3]}
                        stroke="#46A3AA"
                        strokeWidth="2"
                        />
                      )
                    })}

                  {points.map((prop, key) => {
                      return (
                        <Circle
                        cx={prop[0]}
                        cy={prop[1] }
                        r={ 6 }
                        stroke={ '#46A3AA' }
                        strokeWidth={ 2 }
                        fill={ '#E5E5E5' }
                        />  
                      )
                    })}
       
                    </G>
                  </Svg>
                <View style={{flexDirection: "row", justifyContent: "space-evenly", marginTop: 10}}>
                    <Text style={{textAlign:"center", fontFamily:"Pangram-Bold"}}>Feb{"\n"}2017</Text>
                    <Text style={{textAlign:"center", fontFamily:"Pangram-Bold"}}>Mar{"\n"}2017</Text>
                    <Text style={{textAlign:"center", fontFamily:"Pangram-Bold"}}>Jun{"\n"}2017</Text>
                    <Text style={{textAlign:"center", fontFamily:"Pangram-Bold"}}>Jul{"\n"}2017</Text>   
                    <Text style={{textAlign:"center", fontFamily:"Pangram-Bold"}}>Aug{"\n"}2017</Text>
                </View>
            </Animated.View>
        )
    }
}

