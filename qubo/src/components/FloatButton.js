import React, {Component} from 'react';
import { FloatingAction } from "react-native-floating-action";
import Icon from 'react-native-vector-icons/FontAwesome'
import {Text} from 'react-native'

export default class FloatButton extends Component{

    
    state = {
        icon: "bars"
    }
    
    render(){
        const actions = [
            {
              text: <Text style={{fontSize:10}}>Settings</Text>,
              name: "bt_settings",
              position: 1,
              icon: <Icon name="cogs" size={20} color="white"/>,
              color: "#46A3AA",
              textBackground: "black",
              textColor: "white",
              buttonSize: 56,
              margin:0,
            },
            {
                text: <Text style={{fontSize:10}}>Logout</Text>,
                name: "bt_etste",
                position: 2,
                icon: <Icon name="sign-out" size={20} color="white"/>,
                color: "#46A3AA",
                textBackground: "black",
                textColor: "white",
                buttonSize: 56,
                margin:0,
              },
  
          ];
        return(
                <FloatingAction
                    actions={actions}
                    color="#46A3AA"
                    overlayColor= "rgba(255, 255, 255, 0.6)"
                    floatingIcon={<Icon name={this.state.icon} color="white" size={25}/>}
                    onOpen={name =>{
                        this.setState({
                            icon: "times"
                        })
                    }}
                    onClose={name =>{
                        this.setState({
                            icon: "bars"
                        })
                    }}
                />
        )
    }
}

