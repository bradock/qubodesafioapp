import React, {Component} from 'react'
import {Text,View,Animated} from 'react-native'

import {BarStyle,containerApp} from '../styles/Style'

export default class Bar extends Component{

    state = {
        boxWidth: new Animated.Value(0)
    }

    render(){

        this.widthRect = this.props.info.rect;
        this.upRect = this.props.info.up;

        this.colorRect = "#F41616";

        if(this.upRect){
            this.colorRect = "#46A3AA";
        }

        Animated.timing(
            this.state.boxWidth,
            {
                toValue:  this.widthRect = this.props.info.rect,
                duration: 5000
            }
        ).start()

        return(
            <View style={BarStyle.MainBar}>
                <View style={BarStyle.ListBar}>
                    <Text style={[containerApp.TitleBold,{fontSize:12, color: "black", marginLeft: 25}]}>{this.props.info.nome} </Text>
                    <Animated.View style={[BarStyle.Rect, {width: this.state.boxWidth, backgroundColor: this.colorRect}]}>
                    <Text style={[containerApp.TitleBold,{fontSize:9}]}>{this.props.info.sub}</Text>
                    </Animated.View>
                </View>
            </View>
        )
    }
}