import React, {Component} from 'react'
import {Text,View,TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import {BoxStyle} from '../styles/Style'


export default class Box extends Component{

    state = {
        visible: false
    }

    setVisible = () =>{
        this.setState({
            visible: !this.state.visible
        })
    }

    render(){
        return(
            <View style={BoxStyle.MainBox}>
                <TouchableOpacity onPress={() => this.setVisible()}>
                    <View style={BoxStyle.Box}>
                        <Text style={BoxStyle.TitleBox}>{this.props.info.nome}</Text>
                        <View style={BoxStyle.Line}></View>
                        <Icon name="angle-right" size={30} color="white"/>
                    </View>
                </TouchableOpacity>
                {this.state.visible &&(
                    <View style={BoxStyle.BoxIn}>
                        <Text style={BoxStyle.TextBoxIn}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</Text>
                    </View>
                )}      
            </View>
        )
    }
}