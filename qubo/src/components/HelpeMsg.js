import React, {Component} from 'react'
import {Text, View, TouchableOpacity} from 'react-native'

import {HelperMensage} from '../styles/Style'

export default class HelpeMsg extends Component{
    render(){
        return(
            <View style={HelperMensage.MainHelp}>
                <TouchableOpacity>
                <View style={HelperMensage.BoxHelp}>
                    <Text style={HelperMensage.TextHelp}>{this.props.help.msg}</Text>
                </View>
                </TouchableOpacity>
            </View>
        )
    }
}