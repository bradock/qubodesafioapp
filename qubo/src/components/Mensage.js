import React, {Component} from 'react'
import {Text,View} from 'react-native'

import {MensageStyle} from '../styles/Style'

export default class Mensage extends Component{
    render(){

        this.styleBoxMensage = MensageStyle.MensageBoxRemet

        if(this.props.msg.r){
            this.styleBoxMensage = MensageStyle.MensageBoxDest
        }


        return(
        <View style={[MensageStyle.MensageBox, this.styleBoxMensage]}>
            <Text style={MensageStyle.MensageTitle}>{this.props.msg.msg}</Text>
            <Text style={MensageStyle.MensageTime}>{this.props.msg.time}</Text>
        </View>
        
        )
    }
}