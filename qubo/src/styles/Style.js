import React from 'react'
import {StyleSheet,Dimensions} from 'react-native'
import {Fonts} from '../utils/Fonts'

export const containerApp = StyleSheet.create({
    Main:{
        flex: 1,
        backgroundColor: "#E5E5E5"
    },
    Header:{
        backgroundColor: "#232323",
        height: 55,
        alignItems: "center",
        justifyContent: "center"
    },
    TitleHeader:{
        fontFamily: Fonts.Bold,
        fontSize: 22,
        color: "#FFFFFF"
    },
    TitleLight:{
        fontSize: 20,
        color: "#FFFFFF",
        fontFamily: Fonts.Light
    },
    TitleBold:{
        fontSize: 40,
        fontFamily: Fonts.Bold,
        color: "#FFFFFF"
    }
});


export const BoxStyle = StyleSheet.create({
    MainBox:{
        flex: 1,
        marginTop: 0
    },
    Box:{
        backgroundColor: "#232323",
        margin: 10,
        padding: 10,
        height: 72,
        borderRadius: 6,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around"
    },
    TitleBox: {
        color: "#FFFFFF",
        fontSize: 17,
        fontFamily: Fonts.Bold
    },
    Line:{
        borderBottomWidth: 1,
        borderBottomColor: "#FFFFFF",
        width: Dimensions.get("window").width - 240
    },
    BoxIn:{
        backgroundColor: "#46A3AA",
        padding: 10,
        margin: 10,
        borderRadius: 6
    },
    TextBoxIn:{
        fontSize: 18,
        color: "#FFFFFF",
        fontFamily: Fonts.Light,
        padding: 10,
        textAlign: "justify"
    }
})

export const MensageStyle = StyleSheet.create({
    MensageBox:{
        margin: 15,
        padding: 15,
        borderRadius: 6,
        height: 101,
        width: 225.6
    },
    MensageBoxRemet:{
        backgroundColor: "#46A3AA",
        alignSelf: "flex-end"
    },
    MensageBoxDest:{
        backgroundColor: "#232323",
        alignSelf: "flex-start"
    },
    MensageTitle:{
        color: "#FFFFFF",
        fontFamily: Fonts.Bold,
        fontSize: 13
    },
    MensageTime: {
        color: "#FFFFFF",
        fontFamily: Fonts.Light,
        fontSize: 13,
        alignSelf: "flex-end",
        padding: 10
    }
})

export const HelperMensage = StyleSheet.create({
    MainHelp:{
        height: 49,
        justifyContent: "center",
        borderTopWidth: 1,
        borderTopColor: "#DDDDDD",
        borderBottomColor: "#DDDDDD",
        borderBottomWidth: 1
    },
    TextHelp:{
        fontSize: 11,
        color: "#FFFFFF",
        fontFamily: Fonts.Bold,
    },
    BoxHelp:{
        backgroundColor: "#232323",
        marginLeft: 10,
        width: 190.98,
        height: 27,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 24
    }
})


export const InputChat = StyleSheet.create({
    InputBox:{
        backgroundColor: "#FFFFFF",
        height: 78,
        marginTop: 10,
        padding: 20,
        flexDirection: "row",
        justifyContent: "space-between"
    },
    TextInput:{
        fontFamily: Fonts.Bold, 
        fontSize:14, 
        color: "#000000",
        width: 210
    }
})

export const HomeStyle = StyleSheet.create({
    TitleHome:{
        fontSize:30,
        fontFamily: Fonts.Bold,
        padding: 20
    },
    ViewLineHome:{
        borderBottomWidth: 1,
        borderBottomColor: "gray",
        width: Dimensions.get("window").width/2.5
    },
    ViewLineSeparate:{
        borderBottomWidth: 1,
        borderBottomColor: "gray",
        marginTop: 40,
        marginBottom: 40,
        opacity: 0.1
    },
    LegendYear:{
        alignSelf: "flex-end"
    },
    LegendRow:{
        flexDirection: "row",
        marginRight:5
    },
    TextAlignLine:{
        marginRight:30,
        fontSize: 12,
        textAlign: "center"
    },
    LineCashInfo:{
        margin:20,
        alignSelf: "flex-end",
        lineHeight: 30,
        borderBottomColor: "gray",
        borderBottomWidth: 1
    },
    rectLeng:{
        backgroundColor: "#46A3AA",
        width: 19,
        height: 17,
        marginRight: 10,
        marginLeft: 20
    },
    ViewLeng:{
        margin: 20,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    },
    TextRectLeng:{
        fontSize: 14,
        fontFamily: Fonts.Bold,
        color: "black"
    },
    LineLengRect:{
        borderBottomColor: "gray",
        borderBottomWidth: 1,
        width: 280,
        marginLeft: 80
    }
})

export const BarStyle = StyleSheet.create({
    MainBar:{
        flex: 1,
        marginTop: 30
    },
    ListBar:{
        alignSelf: "flex-start",
        flexDirection: "row",
    },
    Rect:{
        backgroundColor: "#46A3AA",
        width: 184.26,
        position: 'absolute',
        left: Dimensions.get("window").width/4,
        height: 16,
        marginLeft: Dimensions.get("window").width/8,
        alignItems: "center",
        justifyContent: "center"
    }
})