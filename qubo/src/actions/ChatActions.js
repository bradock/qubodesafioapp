export const editTextInput = (text) => {
    return {
        type: 'editTextInput',
        payload:{
            textValue: text
        }
    };
}

export const insertChat = (text) => {
    return {
        type: 'insertChat',
        payload: {
            textValue: text
        }
    }
}