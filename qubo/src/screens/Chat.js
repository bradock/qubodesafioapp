import React, {Component} from 'react'
import {Text,View,Dimensions,TouchableOpacity,FlatList,TextInput} from 'react-native'
import {Fonts} from '../utils/Fonts'
import Icon from 'react-native-vector-icons/FontAwesome'
import {containerApp,InputChat} from '../styles/Style'

import Mensage from '../components/Mensage'
import HelpeMsg from '../components/HelpeMsg'

import {connect} from 'react-redux'

import {editTextInput,insertChat} from '../actions/ChatActions'

export class Chat extends Component{

    render(){
        return(
            <View style={containerApp.Main}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("Home")}>
                    <View style={[containerApp.Header, {flexDirection: "row", alignItems:"center", justifyContent:"flex-start"}]}>
                            <Icon name="arrow-left" size={25} color="#FFFFFF" style={{marginLeft: 25}}/>
                            <Text style={[containerApp.TitleHeader,{marginLeft: Dimensions.get("window").width/3.1}]}>Qbot</Text> 
                    </View>
                </TouchableOpacity>

                <FlatList
                data={this.props.chat}
                renderItem={({item}) => <Mensage msg={item}/>}
                />
                
                <FlatList
                horizontal={true}
                data={this.props.helpMsg}
                renderItem={({item}) => <HelpeMsg help={item}/>}
                />

                <View style={InputChat.InputBox}>
                    <Icon name="microphone" size={30} color="#000000"/>
                    <TextInput
                    style={InputChat.TextInput}
                    placeholder="What do you want me to do?"
                    placeholderTextColor="#000000"
                    value={this.props.textValue}
                    onChangeText={(text) => this.props.editTextInput(text)}
                    />
                    <Icon onPress={() => this.props.insertChat(this.props.textValue)} name="share" size={30} color="#000000"/>
                </View>

            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        textValue: state.ChatReducer.textValue,
        chat: state.ChatReducer.chat,
        helpMsg: state.ChatReducer.helpMsg,
        contador: state.ChatReducer.contador
    };
}

const ChatConnect = connect(mapStateToProps, {editTextInput,insertChat})(Chat);
export default ChatConnect;