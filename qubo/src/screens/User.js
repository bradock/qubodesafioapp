import React, {Component} from 'react'
import {Text,View} from 'react-native'

import {containerApp} from '../styles/Style'

export default class User extends Component{
    render(){
        return(
            <View style={[containerApp.Main, {backgroundColor: "#232323", alignItems: "center", justifyContent: "center"}]}>
                    <Text style={containerApp.TitleBold}>Qubo</Text>
                    <Text style={containerApp.TitleLight}>Desafio</Text>
                    <Text style={containerApp.TitleLight}>Francisco Hugo César</Text>
            </View>
        )
    }
}