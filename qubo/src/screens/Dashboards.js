import React, {Component} from 'react'
import {Text,View,FlatList} from 'react-native'

import FloatButton from '../components/FloatButton'
import {containerApp} from '../styles/Style'

import Box from '../components/Box'

import {connect} from 'react-redux'

export class Dashboards extends Component{

    render(){
        return(
            <View style={containerApp.Main}>
                <View style={containerApp.Header}>
                    <Text style={containerApp.TitleHeader}>Dashboards</Text>
                </View>
                
                <FlatList 
                data={this.props.data}
                renderItem={({item}) => <Box info={item} />}
                />

                <FloatButton />
            </View>
        )
    }
}

const mapStateToProps = (state) =>{
    return {
        data: state.DashReducer.data
    }
}

const DashConnect = connect(mapStateToProps)(Dashboards)
export default DashConnect;