import React, {Component} from 'react'
import {Text,View,ScrollView,FlatList} from 'react-native'

import FloatButton from '../components/FloatButton'
import {containerApp,HomeStyle} from '../styles/Style'

import Graphic from '../components/Graphic'
import { Fonts } from '../utils/Fonts'

import Bar from '../components/Bars'

import {connect} from 'react-redux'
import {YellowBox} from 'react-native';

export class Home extends Component{

    render(){
        YellowBox.ignoreWarnings(['Warning: ReactNative.createElement']);
        return(
            <View style={containerApp.Main}>
                <ScrollView >
                    <View style={containerApp.Header}>
                        <Text style={containerApp.TitleHeader}>John</Text>
                    </View>

                    <View>
                        <Text style={HomeStyle.TitleHome}>Your Revenue</Text>
                        <View style={HomeStyle.ViewLineHome}></View>
                    </View>

                    <Graphic/>

                    <View style={HomeStyle.ViewLineSeparate}></View>

                    <View style={HomeStyle.LegendYear}>
                        <View style={HomeStyle.LegendRow}>
                            <Text style={[HomeStyle.TextAlignLine,{fontFamily: Fonts.Bold, borderBottomColor: "gray", borderBottomWidth:1}]}>January</Text>
                            <Text style={[HomeStyle.TextAlignLine,{fontFamily: Fonts.Bold, borderBottomColor: "gray", borderBottomWidth:1}]}>2019</Text>
                        </View>

                        <View style={HomeStyle.LegendRow}>
                            <Text style={[HomeStyle.TextAlignLine,{fontFamily: Fonts.Light}]}>Month</Text>
                            <Text style={[HomeStyle.TextAlignLine,{marginLeft:12,fontFamily: Fonts.Light}]}> Year</Text>
                        </View>
                    </View>

                    <View>
                        <Text style={HomeStyle.TitleHome}>Cash Flow</Text>
                        <View style={HomeStyle.ViewLineHome}></View>
                    </View>

                    <View style={HomeStyle.ViewLeng}>
                        <View style={HomeStyle.rectLeng}></View>
                        <Text style={HomeStyle.TextRectLeng}>Cash Received</Text>
                        <View style={[HomeStyle.rectLeng, {backgroundColor: "#F41616"}]}></View>
                        <Text style={HomeStyle.TextRectLeng}>Cash Spent </Text>
                    </View>

                    <View style={HomeStyle.LineLengRect}></View>

                    <View style={HomeStyle.LineCashInfo}>
                        <Text>
                            $0M {'\u00A0'} 
                            $.50M  {'\u00A0'} 
                            $1M  {'\u00A0'} 
                            $1.5M {'\u00A0'}
                            $2M {'\u00A0'}  
                        </Text>
                    </View>

                    <FlatList 
                    data={this.props.bars}
                    renderItem={({item}) => <Bar info={item} />}
                    />

                <View style={HomeStyle.ViewLineSeparate}></View>

                </ScrollView>

                <FloatButton/>
            </View>
        )
    }
}

const mapStateToProps = (state) =>{
    return {
        bars:state.HomeReducer.bars
    }
}

const connectHome = connect(mapStateToProps)(Home);

export default connectHome;