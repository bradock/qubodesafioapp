import React, {Component} from 'react'
import {createBottomTabNavigator, createAppContainer} from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome'

import Home from './src/screens/Home'
import Dashboards from './src/screens/Dashboards'
import User from './src/screens/User'
import Chat from './src/screens/Chat'

import {Fonts} from './src/utils/Fonts'
import Reducers from './src/utils/Reducers'

import {Provider} from 'react-redux'
import {createStore} from 'redux'

let store = createStore(Reducers);

const AppNavigatorList = createBottomTabNavigator({
    Home:{
      screen:Home,
      navigationOptions:{
        tabBarLabel: "Home",
        tabBarIcon: ({tintColor}) => (
          <Icon name="home" size={20} color={tintColor}/>
        )
      }
    },
    Dashboards:{
      screen:Dashboards,
      navigationOptions:{
        tabBarLabel: "Dashboards",
        tabBarIcon: ({tintColor}) => (
          <Icon name="th-large" size={20} color={tintColor}/>
        )
      }
    },
    User:{
      screen:User,
      navigationOptions:{
        tabBarLabel: "Profile",
        tabBarIcon: ({tintColor}) => (
          <Icon name="user" size={20} color={tintColor}/>
        )
      }
    },
    Chat:{
      screen:Chat,
      navigationOptions:{
        tabBarLabel: "Chat",
        tabBarVisible: false,
        tabBarIcon: ({tintColor}) => (
          <Icon name="comment" size={20} color={tintColor}/>
        )
      }
    }
},
{
  tabBarOptions:{
    activeTintColor: "#46A3AA",
    inactiveTintColor: "#6D6D6D",
    style:{
      backgroundColor: "white",
      height: 60
    },
    labelStyle:{
      fontSize: 13,
      fontFamily: Fonts.Bold
    }
  }
}
)

const Navig =  createAppContainer(AppNavigatorList);


export default class AppNavigator extends Component{
  render(){
    return(
      <Provider store={store}>
          <Navig/>
      </Provider>
    )
  }
}

//export default createAppContainer(AppNavigator);